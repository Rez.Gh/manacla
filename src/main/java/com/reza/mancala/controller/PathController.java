package com.reza.mancala.controller;

import com.reza.mancala.domain.board.Board;
import com.reza.mancala.domain.gameplay.GamePlayService;
import com.reza.mancala.domain.player.Player;
import com.reza.mancala.domain.player.RegisterDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;


@Controller
public class PathController {

    @Autowired
    private GamePlayService gamePlayService;


    @GetMapping("/board")
    public String getBoardPage(RegisterDTO registerDTO, Model model) {

        Board board = (Board) model.getAttribute("board");
        model.addAttribute("board", board);
        return "board";

    }

    @PostMapping("/start")
    public String startGame(RegisterDTO dto, Model model) {

        model.addAttribute("board", gamePlayService.startGame(dto.getPlayerOneName(), dto.getPlayerTwoName()));

        return "/board";
    }

    @GetMapping("/play/{id}/{pitNumber}/{stoneCount}")
    public String Play(Model model, @ModelAttribute(value = "board") Board board, @PathVariable Integer id, @PathVariable Integer pitNumber, @PathVariable Integer stoneCount) {
        board = (Board) model.getAttribute("board");
        Player player = null;
        if (id == 1) {
            player = board.getPlayerOne();
        } else if (id == 2) {
            player = board.getPlayerTwo();
        }
        model.addAttribute("board", gamePlayService.playGame(player, pitNumber, stoneCount));

        return "board";

    }

}
