package com.reza.mancala.domain.gameplay;

import com.reza.mancala.domain.board.Board;
import com.reza.mancala.domain.player.Player;
import org.springframework.stereotype.Service;

@Service
public interface GamePlayService {

    Board startGame(String playerOneName, String PlayerTwoName);

    Board playGame(Player player, Integer pit, Integer stoneCount);

    boolean endGame(Board board);

    Player getWinner(Board board);
}
