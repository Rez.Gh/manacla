package com.reza.mancala.domain.gameplay;

import com.reza.mancala.domain.board.Board;
import com.reza.mancala.domain.board.BoardService;
import com.reza.mancala.domain.board.pit.SmallPit;
import com.reza.mancala.domain.board.stone.Stone;
import com.reza.mancala.domain.player.Player;
import com.reza.mancala.domain.player.PlayerService;
import com.reza.mancala.domain.rule.RulesGameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;

@Service
public class DefaultGamePlayService implements GamePlayService {

    @Autowired
    private PlayerService playerService;

    @Autowired
    private BoardService boardService;

    @Autowired
    private RulesGameService rulesGameService;

    private Board board;
    private Player playerOne;
    private Player playerTwo;

    @Override
    public Board startGame(String playerOneName, String playerTwoName) {

        playerOne = playerService.createPlayer(playerOneName, 1);
        playerTwo = playerService.createPlayer(playerTwoName, 2);
        board = boardService.createBoard(playerOne, playerTwo, 12, 6);
        return board;
    }

    @Override
    public Board playGame(Player player, Integer pitNumber, Integer stoneCount) {

        if (player.isGameTurn()) {
            if (player.getPlayerId() == 1) {
                if (pitNumber >= (board.getSmallPitCount() / 2)) {
                    String warning = "Please select own pit !";
                    board.setWarning(warning);
                    return board;
                } else {
                    SmallPit smallPit = board.getPlayerOneSmallPits().get(pitNumber - 1);
                    LinkedList<Stone> stones = smallPit.getStones();
                    Boolean isEmptySmallPit = stones.isEmpty();

                    if (isEmptySmallPit) {
                        String warning = "This pit empty please select another pit";
                        board.setWarning(warning);
                        return board;
                    }

                    for (int i = 0; i < board.getPlayerOneSmallPits().size() - pitNumber; i++) {
                        board.getPlayerOneSmallPits().get(pitNumber + i).getStones().add(stones.poll());

                        if (isEmptySmallPit) {

                            rulesGameService.capturingStones(board, board.getPlayerOne(),
                                    board.getPlayerOneSmallPits().get(pitNumber + i), pitNumber + i);

                            if (endGame(board)) {
                                rulesGameService.capturingRemindsStone(board, board.getPlayerOne());
                                getWinner(board);
                                return board;
                            }
                            board.getPlayerOne().setGameTurn(false);
                            board.getPlayerTwo().setGameTurn(true);
                            return board;
                        }
                    }
                    if (!isEmptySmallPit) {
                        while (!isEmptySmallPit) {
                            board.getPlayerOneBigPit().getStones().add(stones.poll());
                            if (!isEmptySmallPit) {
                                for (int i = 0; i < board.getPlayerTwoSmallPits().size(); i++) {
                                    board.getPlayerTwoSmallPits().get(i).getStones().add(stones.poll());
                                    if (isEmptySmallPit) {

                                        if (endGame(board)) {
                                            rulesGameService.capturingRemindsStone(board, board.getPlayerOne());
                                            getWinner(board);
                                            return board;
                                        }
                                        board.getPlayerOne().setGameTurn(false);
                                        board.getPlayerTwo().setGameTurn(true);
                                        return board;
                                    }
                                }
                            } else if (isEmptySmallPit) {

                                if (endGame(board)) {
                                    rulesGameService.capturingRemindsStone(board, board.getPlayerOne());
                                    getWinner(board);
                                    return board;
                                }
                                board.getPlayerOne().setGameTurn(true);
                                return board;
                            }

                            if (!isEmptySmallPit) {
                                for (int i = 0; i < board.getPlayerOneSmallPits().size(); i++) {

                                    board.getPlayerOneSmallPits().get(i).getStones().add(stones.poll());

                                    if (isEmptySmallPit) {

                                        rulesGameService.capturingStones(board, board.getPlayerOne(),
                                                board.getPlayerOneSmallPits().get(pitNumber + i), pitNumber + i);

                                        if (endGame(board)) {
                                            rulesGameService.capturingRemindsStone(board, board.getPlayerOne());
                                            getWinner(board);
                                            return board;
                                        }

                                        board.getPlayerOne().setGameTurn(false);
                                        board.getPlayerTwo().setGameTurn(true);
                                        return board;
                                    }
                                }
                            } else if (isEmptySmallPit) {

                                if (endGame(board)) {
                                    rulesGameService.capturingRemindsStone(board, board.getPlayerOne());
                                    getWinner(board);
                                    return board;
                                }

                                board.getPlayerOne().setGameTurn(false);
                                board.getPlayerTwo().setGameTurn(true);
                                return board;
                            }
                        }
                    }
                }

            } else if (player.getPlayerId() == 2) {
                if (pitNumber <= (board.getSmallPitCount() / 2)) {
                    String warning = "Please select own pit !";
                    board.setWarning(warning);
                    return board;
                } else {
                    SmallPit smallPit = board.getPlayerTwoSmallPits().get(pitNumber - 1);
                    LinkedList<Stone> stones = smallPit.getStones();
                    Boolean isEmptySmallPit = stones.isEmpty();

                    if (isEmptySmallPit) {
                        String warning = "This pit empty please select another pit";
                        board.setWarning(warning);
                        return board;
                    }

                    for (int i = 0; i < board.getPlayerTwoSmallPits().size() - pitNumber; i++) {
                        board.getPlayerTwoSmallPits().get(pitNumber + i).getStones().add(stones.poll());

                        if (isEmptySmallPit) {

                            rulesGameService.capturingStones(board, board.getPlayerTwo(),
                                    board.getPlayerTwoSmallPits().get(pitNumber + i), pitNumber + i);

                            if (endGame(board)) {
                                rulesGameService.capturingRemindsStone(board, board.getPlayerTwo());
                                getWinner(board);
                                return board;
                            }
                            board.getPlayerTwo().setGameTurn(false);
                            board.getPlayerOne().setGameTurn(true);
                            return board;
                        }
                    }
                    if (!isEmptySmallPit) {
                        while (!isEmptySmallPit) {
                            board.getPlayerTwoBigPit().getStones().add(stones.poll());
                            if (!isEmptySmallPit) {
                                for (int i = 0; i < board.getPlayerOneSmallPits().size(); i++) {
                                    board.getPlayerOneSmallPits().get(i).getStones().add(stones.poll());
                                    if (isEmptySmallPit) {
                                        if (endGame(board)) {
                                            rulesGameService.capturingRemindsStone(board, board.getPlayerTwo());
                                            getWinner(board);
                                            return board;
                                        }
                                        board.getPlayerTwo().setGameTurn(false);
                                        board.getPlayerOne().setGameTurn(true);
                                        return board;
                                    }
                                }
                            } else if (isEmptySmallPit) {

                                if (endGame(board)) {
                                    rulesGameService.capturingRemindsStone(board, board.getPlayerTwo());
                                    getWinner(board);
                                    return board;
                                }
                                board.getPlayerTwo().setGameTurn(true);
                                return board;
                            }

                            if (!isEmptySmallPit) {
                                for (int i = 0; i < board.getPlayerTwoSmallPits().size(); i++) {

                                    board.getPlayerTwoSmallPits().get(i).getStones().add(stones.poll());

                                    if (isEmptySmallPit) {

                                        rulesGameService.capturingStones(board, board.getPlayerTwo(),
                                                board.getPlayerTwoSmallPits().get(pitNumber + i), pitNumber + i);

                                        if (endGame(board)) {
                                            rulesGameService.capturingRemindsStone(board, board.getPlayerTwo());
                                            getWinner(board);
                                            return board;
                                        }
                                        board.getPlayerTwo().setGameTurn(false);
                                        board.getPlayerOne().setGameTurn(true);
                                        return board;
                                    }
                                }
                            } else if (isEmptySmallPit) {

                                if (endGame(board)) {
                                    rulesGameService.capturingRemindsStone(board, board.getPlayerTwo());
                                    getWinner(board);
                                    return board;
                                }

                                board.getPlayerTwo().setGameTurn(false);
                                board.getPlayerOne().setGameTurn(true);
                                return board;
                            }
                        }
                    }
                }
            }
        } else {
            String warning = "Please play in your turn";
            board.setWarning(warning);
            return board;
        }
        return board;
    }

    @Override
    public boolean endGame(Board board) {

        if (!board.getPlayerOneSmallPits().stream().noneMatch(x -> x.getStones().isEmpty()) ||
                !board.getPlayerTwoSmallPits().stream().noneMatch(x -> x.getStones().isEmpty())) {
            board.setGameEnded(true);
            return true;
        }
        return false;
    }

    @Override
    public Player getWinner(Board board) {
        Player winner = null;
        if (board.getPlayerOneBigPit().getStones().size() > board.getPlayerTwoBigPit().getStones().size()) {
            board.getPlayerOne().setWin(true);
            winner = board.getPlayerOne();
            return winner;
        }

        board.getPlayerTwo().setWin(true);
        winner = board.getPlayerTwo();
        board.getPlayerOne().setGameTurn(false);
        board.getPlayerTwo().setGameTurn(false);
        return winner;
    }
}
