package com.reza.mancala.domain.board;

import com.reza.mancala.domain.board.pit.BigPit;
import com.reza.mancala.domain.board.pit.SmallPit;
import com.reza.mancala.domain.player.Player;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.LinkedList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Board {
    private Integer smallPitCount;
    private Integer smallPitCapacity;
    private Player playerOne;
    private Player playerTwo;
    private BigPit playerOneBigPit;
    private BigPit playerTwoBigPit;
    private List<SmallPit> playerOneSmallPits = new LinkedList<>();
    private List<SmallPit> playerTwoSmallPits = new LinkedList<>();
    private boolean gameEnded;
    private String warning;
}
