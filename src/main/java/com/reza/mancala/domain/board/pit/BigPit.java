package com.reza.mancala.domain.board.pit;

import com.reza.mancala.domain.board.stone.Stone;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BigPit {
    private List<Stone> stones = new ArrayList<>();
}
