package com.reza.mancala.domain.board;

import com.reza.mancala.domain.board.pit.BigPit;
import com.reza.mancala.domain.board.pit.SmallPit;
import com.reza.mancala.domain.player.Player;
import com.reza.mancala.domain.board.stone.Stone;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Service
public class DefaultBoardService implements BoardService {
    @Override
    public Board createBoard(Player playerOne, Player playerTwo,Integer smallPitCount , Integer smallPitCapacity) {

        Board board = new Board();
        board.setPlayerOne(playerOne);
        board.setPlayerTwo(playerTwo);
        board.setPlayerOneBigPit(new BigPit());
        board.setPlayerTwoBigPit(new BigPit());

        LinkedList<Stone> stones = new LinkedList<>();
        for (int i = 0; i < smallPitCapacity ; i++) {
            stones.add(new Stone());
        }

        List<SmallPit> playerOneSmallPits = new ArrayList<>();
        List<SmallPit> playerTwoSmallPits = new ArrayList<>();
        for (int i = 0; i < smallPitCount / 2 ; i++) {
            playerOneSmallPits.add(new SmallPit(stones));
            playerTwoSmallPits.add(new SmallPit(stones));
        }

        board.setPlayerOneSmallPits(playerOneSmallPits);
        board.setPlayerTwoSmallPits(playerTwoSmallPits);

        return board;
    }
}
