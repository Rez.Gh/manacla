package com.reza.mancala.domain.board.pit;

import com.reza.mancala.domain.board.stone.Stone;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.LinkedList;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SmallPit {
    private LinkedList<Stone> stones = new LinkedList<>();
}
