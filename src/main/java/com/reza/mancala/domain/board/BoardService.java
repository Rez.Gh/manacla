package com.reza.mancala.domain.board;

import com.reza.mancala.domain.player.Player;
import org.springframework.stereotype.Service;

@Service
public interface BoardService {

    Board createBoard(Player playerOne, Player playerTwo , Integer smallPitCount , Integer smallPitCapacity);
}
