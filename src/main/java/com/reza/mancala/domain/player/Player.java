package com.reza.mancala.domain.player;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Player {

    private String name;
    private Integer playerId;
    private boolean gameTurn;
    private boolean win;
}
