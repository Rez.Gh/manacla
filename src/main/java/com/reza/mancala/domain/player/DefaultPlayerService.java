package com.reza.mancala.domain.player;

import org.springframework.stereotype.Service;

@Service
public class DefaultPlayerService implements PlayerService {

    @Override
    public Player createPlayer(String name, Integer id) {

        Player player = new Player();
        player.setName(name);
        player.setPlayerId(id);
        if (id == 1) {
            player.setGameTurn(true);
            return player;
        }
        player.setGameTurn(false);
        return player;
    }
}
