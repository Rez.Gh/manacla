package com.reza.mancala.domain.player;

import org.springframework.stereotype.Service;

@Service
public interface PlayerService {

    Player createPlayer(String name, Integer id);
}
