package com.reza.mancala.domain.rule;

import com.reza.mancala.domain.board.Board;
import com.reza.mancala.domain.board.pit.SmallPit;
import com.reza.mancala.domain.player.Player;
import com.reza.mancala.domain.board.stone.Stone;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface RulesGameService {

    Boolean bonusTurn(Boolean bonusCondition);

    void capturingStones(Board board, Player player, SmallPit smallPit, Integer index);

    List<Stone> capturingRemindsStone(Board board, Player player);
}
