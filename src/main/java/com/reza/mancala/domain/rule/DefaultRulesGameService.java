package com.reza.mancala.domain.rule;

import com.reza.mancala.domain.board.Board;
import com.reza.mancala.domain.board.pit.SmallPit;
import com.reza.mancala.domain.player.Player;
import com.reza.mancala.domain.board.stone.Stone;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DefaultRulesGameService implements RulesGameService {

    @Override
    public Boolean bonusTurn(Boolean bonusCondition) {
        if (bonusCondition){

        }
        return null;
    }

    @Override
    public void capturingStones(Board board, Player player, SmallPit smallPit, Integer index) {

        if (smallPit.getStones().size() == 1) {

            switch (player.getPlayerId()) {
                case 1:
                    board.getPlayerOneBigPit().getStones()
                            .add(smallPit.getStones().poll());

                    board.getPlayerOneBigPit().getStones()
                            .addAll(smallPit.getStones());

                    board.getPlayerTwoSmallPits().set(index, new SmallPit());
                    break;
                case 2:
                    board.getPlayerTwoBigPit().getStones()
                            .add(smallPit.getStones().poll());

                    board.getPlayerTwoBigPit().getStones()
                            .addAll(smallPit.getStones());

                    board.getPlayerOneSmallPits().set(index, new SmallPit());
                    break;
            }
        }
    }

    @Override
    public List<Stone> capturingRemindsStone(Board board, Player player) {

        List<Stone> stones = new ArrayList<>();

        switch (player.getPlayerId()) {
            case 1:
                for (SmallPit smallPit : board.getPlayerOneSmallPits()) {
                    if (smallPit.getStones() != null && !smallPit.getStones().isEmpty()) {
                        stones.addAll(smallPit.getStones());
                    }
                }
                break;
            case 2:
                for (SmallPit smallPit : board.getPlayerTwoSmallPits()) {
                    if (smallPit.getStones() != null && !smallPit.getStones().isEmpty()) {
                        stones.addAll(smallPit.getStones());
                    }
                }
                break;
        }
        return stones;
    }
}
